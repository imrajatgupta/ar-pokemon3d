xof 0303txt 0032

template AnimTicksPerSecond {
  <9E415A43-7BA6-4a73-8743-B73D47E88476>
  DWORD AnimTicksPerSecond;
}

template XSkinMeshHeader {
  <3cf169ce-ff7c-44ab-93c0-f78f62d172e2>
  WORD nMaxSkinWeightsPerVertex;
  WORD nMaxSkinWeightsPerFace;
  WORD nBones;
}

template SkinWeights {
  <6f0d123b-bad2-4167-a0d0-80224f25fabb>
  STRING transformNodeName;
  DWORD nWeights;
  array DWORD vertexIndices[nWeights];
  array float weights[nWeights];
  Matrix4x4 matrixOffset;
}

Frame Root {
  FrameTransformMatrix {
     1.000000, 0.000000, 0.000000, 0.000000,
     0.000000, 0.000000, 1.000000, 0.000000,
     0.000000, 1.000000,-0.000000, 0.000000,
     0.000000, 0.000000, 0.000000, 1.000000;;
  }
  Frame naty_mat_0 {
    FrameTransformMatrix {
       1.000000, 0.000000, 0.000000, 0.000000,
       0.000000, 1.000000, 0.000000, 0.000000,
       0.000000, 0.000000, 1.000000, 0.000000,
       0.000000, 0.000000, 0.000000, 1.000000;;
    }
    Mesh { //Mesh Mesh
      12;
       0.657027; 1.129279;-0.636396;,
       0.330957; 1.217559;-0.831492;,
       0.355580; 1.132639;-0.831492;,
       0.657027; 1.129279;-0.636396;,
       0.355580; 1.132639;-0.831492;,
       0.792770; 1.432789;-0.344415;,
       0.657027; 1.129279;-0.636396;,
       0.792770; 1.432789;-0.344415;,
       0.849507; 1.456861; 0.000000;,
       0.792770; 1.432789;-0.344415;,
       0.929176; 1.134429; 0.000000;,
       0.849507; 1.456861; 0.000000;;
      4;
      3;0;1;2;,
      3;3;4;5;,
      3;6;7;8;,
      3;9;10;11;;
      MeshNormals { //Mesh Normals
        12;
         0.576434; 0.443464;-0.686300;,
        -0.537889;-0.155950; 0.828425;,
        -0.543352;-0.025666; 0.839106;,
         0.576434; 0.443464;-0.686300;,
        -0.543352;-0.025666; 0.839106;,
         0.899167;-0.357158;-0.252754;,
         0.576434; 0.443464;-0.686300;,
         0.899167;-0.357158;-0.252754;,
        -0.899380;-0.400067; 0.176122;,
         0.899167;-0.357158;-0.252754;,
        -0.955992;-0.236213; 0.173986;,
        -0.899380;-0.400067; 0.176122;;
        4;
        3;0;1;2;,
        3;3;4;5;,
        3;6;7;8;,
        3;9;10;11;;
      } //End of Mesh Normals
      MeshMaterialList { //Mesh Material List
        1;
        4;
        0,
        0,
        0,
        0;;
        Material mat_0 {
           0.640000; 0.640000; 0.640000; 1.000000;;
           92.156863;
           0.250000; 0.250000; 0.250000;;
           0.000000; 0.000000; 0.000000;;
        }
      } //End of Mesh Material List
      MeshTextureCoords { //Mesh UV Coordinates
        12;
         0.526914; 0.025442;,
         0.903566; 0.088680;,
         0.197308; 0.105018;,
         0.197308; 0.105018;,
         0.903566; 0.088680;,
         0.159354; 0.149995;,
         0.197308; 0.105018;,
         0.159354; 0.149995;,
         0.197000; 0.105000;,
         0.159354; 0.149995;,
         0.574000; 0.090500;,
         0.197000; 0.105000;;
      } //End of Mesh UV Coordinates
    } //End of Mesh Mesh
  } //End of naty_mat_0
  Frame naty_mat_2 {
    FrameTransformMatrix {
       1.000000, 0.000000, 0.000000, 0.000000,
       0.000000, 1.000000, 0.000000, 0.000000,
       0.000000, 0.000000, 1.000000, 0.000000,
       0.000000, 0.000000, 0.000000, 1.000000;;
    }
    Mesh { //Mesh Mesh
      132;
       0.553742; 1.331969; 0.688789;,
       0.718565; 1.401024; 0.490406;,
       0.617605; 1.124693; 0.688789;,
       0.718565; 1.401024; 0.490406;,
       0.788047; 1.130583; 0.490406;,
       0.617605; 1.124693; 0.688789;,
       0.581712; 0.841230; 0.688789;,
       0.617605; 1.124693; 0.688789;,
       0.788047; 1.130583; 0.490406;,
       0.788047; 1.130583; 0.490406;,
       0.741594; 0.812588; 0.490406;,
       0.581712; 0.841230; 0.688789;,
       0.553742; 1.331969; 0.688789;,
       0.617605; 1.124693; 0.688789;,
       0.330957; 1.217559; 0.864632;,
       0.330957; 1.217559; 0.864632;,
       0.617605; 1.124693; 0.688789;,
       0.351811; 1.078555; 0.864632;,
       0.330957; 1.217559; 0.864632;,
       0.351811; 1.078555; 0.864632;,
       0.155994; 1.143193; 0.924649;,
       0.351811; 1.078555; 0.864632;,
       0.268542; 1.035126; 0.880464;,
       0.155994; 1.143193; 0.924649;,
       0.351811; 1.078555; 0.864632;,
       0.617605; 1.124693; 0.688789;,
       0.581712; 0.841230; 0.688789;,
       0.351811; 1.078555; 0.864632;,
       0.581712; 0.841230; 0.688789;,
       0.304444; 0.935321; 0.867515;,
      -0.304445; 0.935322; 0.867515;,
      -0.581713; 0.841232; 0.688789;,
      -0.351812; 1.078556; 0.864632;,
      -0.581713; 0.841232; 0.688789;,
      -0.617606; 1.124694; 0.688789;,
      -0.351812; 1.078556; 0.864632;,
      -0.351812; 1.078556; 0.864632;,
      -0.617606; 1.124694; 0.688789;,
      -0.330957; 1.217560; 0.864632;,
      -0.617606; 1.124694; 0.688789;,
      -0.553741; 1.331970; 0.688789;,
      -0.330957; 1.217560; 0.864632;,
      -0.330957; 1.217560; 0.864632;,
      -0.553741; 1.331970; 0.688789;,
      -0.261914; 1.326911; 0.862575;,
      -0.553741; 1.331970; 0.688789;,
      -0.445431; 1.509330; 0.688789;,
      -0.261914; 1.326911; 0.862575;,
      -0.445431; 1.509330; 0.688789;,
      -0.553741; 1.331970; 0.688789;,
      -0.565349; 1.617407; 0.490406;,
      -0.553741; 1.331970; 0.688789;,
      -0.718565; 1.401026; 0.490406;,
      -0.565349; 1.617407; 0.490406;,
      -0.565349; 1.617407; 0.490406;,
      -0.718565; 1.401026; 0.490406;,
      -0.643521; 1.698646; 0.259930;,
      -0.718565; 1.401026; 0.490406;,
      -0.824387; 1.446010; 0.259930;,
      -0.643521; 1.698646; 0.259930;,
      -0.553741; 1.331970; 0.688789;,
      -0.617606; 1.124694; 0.688789;,
      -0.718565; 1.401026; 0.490406;,
      -0.617606; 1.124694; 0.688789;,
      -0.788047; 1.130584; 0.490406;,
      -0.718565; 1.401026; 0.490406;,
      -0.718565; 1.401026; 0.490406;,
      -0.788047; 1.130584; 0.490406;,
      -0.824387; 1.446010; 0.259930;,
      -0.788047; 1.130584; 0.490406;,
      -0.901885; 1.131889; 0.259930;,
      -0.824387; 1.446010; 0.259930;,
      -0.617606; 1.124694; 0.688789;,
      -0.581713; 0.841232; 0.688789;,
      -0.788047; 1.130584; 0.490406;,
      -0.581713; 0.841232; 0.688789;,
      -0.741595; 0.812590; 0.490406;,
      -0.788047; 1.130584; 0.490406;,
      -0.788047; 1.130584; 0.490406;,
      -0.741595; 0.812590; 0.490406;,
      -0.901885; 1.131889; 0.259930;,
      -0.741595; 0.812590; 0.490406;,
      -0.832815; 0.784896; 0.259930;,
      -0.901885; 1.131889; 0.259930;,
      -0.268543; 1.035128; 0.880464;,
      -0.351812; 1.078556; 0.864632;,
      -0.330957; 1.217560; 0.864632;,
      -0.268543; 1.035128; 0.880464;,
      -0.330957; 1.217560; 0.864632;,
      -0.155994; 1.143194; 0.924649;,
       0.261914; 1.326910; 0.862575;,
       0.330957; 1.217559; 0.864632;,
       0.155994; 1.143193; 0.924649;,
      -0.155994; 1.143194; 0.924649;,
      -0.330957; 1.217560; 0.864632;,
      -0.261914; 1.326911; 0.862575;,
      -0.304445; 0.935322; 0.867515;,
      -0.351812; 1.078556; 0.864632;,
      -0.268543; 1.035128; 0.880464;,
       0.268542; 1.035126; 0.880464;,
       0.351811; 1.078555; 0.864632;,
       0.304444; 0.935321; 0.867515;,
       0.643522; 1.698645; 0.259930;,
       0.824388; 1.446008; 0.259930;,
       0.565350; 1.617406; 0.490406;,
       0.824388; 1.446008; 0.259930;,
       0.718565; 1.401024; 0.490406;,
       0.565350; 1.617406; 0.490406;,
       0.565350; 1.617406; 0.490406;,
       0.718565; 1.401024; 0.490406;,
       0.445431; 1.509329; 0.688789;,
       0.718565; 1.401024; 0.490406;,
       0.553742; 1.331969; 0.688789;,
       0.445431; 1.509329; 0.688789;,
       0.445431; 1.509329; 0.688789;,
       0.553742; 1.331969; 0.688789;,
       0.261914; 1.326910; 0.862575;,
       0.553742; 1.331969; 0.688789;,
       0.330957; 1.217559; 0.864632;,
       0.261914; 1.326910; 0.862575;,
       0.832813; 0.784894; 0.259930;,
       0.741594; 0.812588; 0.490406;,
       0.901884; 1.131888; 0.259930;,
       0.741594; 0.812588; 0.490406;,
       0.788047; 1.130583; 0.490406;,
       0.901884; 1.131888; 0.259930;,
       0.901884; 1.131888; 0.259930;,
       0.788047; 1.130583; 0.490406;,
       0.824388; 1.446008; 0.259930;,
       0.788047; 1.130583; 0.490406;,
       0.718565; 1.401024; 0.490406;,
       0.824388; 1.446008; 0.259930;;
      44;
      3;0;1;2;,
      3;3;4;5;,
      3;6;7;8;,
      3;9;10;11;,
      3;12;13;14;,
      3;15;16;17;,
      3;18;19;20;,
      3;21;22;23;,
      3;24;25;26;,
      3;27;28;29;,
      3;30;31;32;,
      3;33;34;35;,
      3;36;37;38;,
      3;39;40;41;,
      3;42;43;44;,
      3;45;46;47;,
      3;48;49;50;,
      3;51;52;53;,
      3;54;55;56;,
      3;57;58;59;,
      3;60;61;62;,
      3;63;64;65;,
      3;66;67;68;,
      3;69;70;71;,
      3;72;73;74;,
      3;75;76;77;,
      3;78;79;80;,
      3;81;82;83;,
      3;84;85;86;,
      3;87;88;89;,
      3;90;91;92;,
      3;93;94;95;,
      3;96;97;98;,
      3;99;100;101;,
      3;102;103;104;,
      3;105;106;107;,
      3;108;109;110;,
      3;111;112;113;,
      3;114;115;116;,
      3;117;118;119;,
      3;120;121;122;,
      3;123;124;125;,
      3;126;127;128;,
      3;129;130;131;;
      MeshNormals { //Mesh Normals
        132;
        -0.606952;-0.276498;-0.745079;,
        -0.759484;-0.350383;-0.548051;,
        -0.669088;-0.051759;-0.741356;,
        -0.759484;-0.350383;-0.548051;,
        -0.837855;-0.045503;-0.543962;,
        -0.669088;-0.051759;-0.741356;,
        -0.666555; 0.100803;-0.738578;,
        -0.669088;-0.051759;-0.741356;,
        -0.837855;-0.045503;-0.543962;,
        -0.837855;-0.045503;-0.543962;,
        -0.842555; 0.138035;-0.520585;,
        -0.666555; 0.100803;-0.738578;,
        -0.606952;-0.276498;-0.745079;,
        -0.669088;-0.051759;-0.741356;,
        -0.416425;-0.169927;-0.893124;,
        -0.416425;-0.169927;-0.893124;,
        -0.669088;-0.051759;-0.741356;,
        -0.421949; 0.031861;-0.906034;,
        -0.416425;-0.169927;-0.893124;,
        -0.421949; 0.031861;-0.906034;,
        -0.275124;-0.049074;-0.960143;,
        -0.421949; 0.031861;-0.906034;,
        -0.232826; 0.093539;-0.967986;,
        -0.275124;-0.049074;-0.960143;,
        -0.421949; 0.031861;-0.906034;,
        -0.669088;-0.051759;-0.741356;,
        -0.666555; 0.100803;-0.738578;,
        -0.421949; 0.031861;-0.906034;,
        -0.666555; 0.100803;-0.738578;,
        -0.417615; 0.119968;-0.900632;,
         0.417615; 0.119968;-0.900632;,
         0.666555; 0.100803;-0.738578;,
         0.390332; 0.015564;-0.920530;,
         0.666555; 0.100803;-0.738578;,
         0.669088;-0.051759;-0.741356;,
         0.390332; 0.015564;-0.920530;,
         0.390332; 0.015564;-0.920530;,
         0.669088;-0.051759;-0.741356;,
         0.411481;-0.157750;-0.897641;,
         0.669088;-0.051759;-0.741356;,
         0.608509;-0.285562;-0.740349;,
         0.411481;-0.157750;-0.897641;,
         0.411481;-0.157750;-0.897641;,
         0.608509;-0.285562;-0.740349;,
         0.397626;-0.261788;-0.879360;,
         0.608509;-0.285562;-0.740349;,
         0.591113;-0.360973;-0.721274;,
         0.397626;-0.261788;-0.879360;,
         0.591113;-0.360973;-0.721274;,
         0.608509;-0.285562;-0.740349;,
         0.693869;-0.478133;-0.538408;,
         0.608509;-0.285562;-0.740349;,
         0.754997;-0.354167;-0.551805;,
         0.693869;-0.478133;-0.538408;,
         0.693869;-0.478133;-0.538408;,
         0.754997;-0.354167;-0.551805;,
         0.733360;-0.522080;-0.435408;,
         0.754997;-0.354167;-0.551805;,
         0.813410;-0.375469;-0.444227;,
         0.733360;-0.522080;-0.435408;,
         0.608509;-0.285562;-0.740349;,
         0.669088;-0.051759;-0.741356;,
         0.754997;-0.354167;-0.551805;,
         0.669088;-0.051759;-0.741356;,
         0.837855;-0.045503;-0.543962;,
         0.754997;-0.354167;-0.551805;,
         0.754997;-0.354167;-0.551805;,
         0.837855;-0.045503;-0.543962;,
         0.813410;-0.375469;-0.444227;,
         0.837855;-0.045503;-0.543962;,
         0.901669;-0.032136;-0.431196;,
         0.813410;-0.375469;-0.444227;,
         0.669088;-0.051759;-0.741356;,
         0.666555; 0.100803;-0.738578;,
         0.837855;-0.045503;-0.543962;,
         0.666555; 0.100803;-0.738578;,
         0.842555; 0.138035;-0.520585;,
         0.837855;-0.045503;-0.543962;,
         0.837855;-0.045503;-0.543962;,
         0.842555; 0.138035;-0.520585;,
         0.901669;-0.032136;-0.431196;,
         0.842555; 0.138035;-0.520585;,
         0.906919; 0.180517;-0.380627;,
         0.901669;-0.032136;-0.431196;,
         0.245247; 0.028993;-0.968993;,
         0.390332; 0.015564;-0.920530;,
         0.411481;-0.157750;-0.897641;,
         0.245247; 0.028993;-0.968993;,
         0.411481;-0.157750;-0.897641;,
         0.308298;-0.041627;-0.950346;,
        -0.397626;-0.261788;-0.879360;,
        -0.416425;-0.169927;-0.893124;,
        -0.275124;-0.049074;-0.960143;,
         0.308298;-0.041627;-0.950346;,
         0.411481;-0.157750;-0.897641;,
         0.397626;-0.261788;-0.879360;,
         0.417615; 0.119968;-0.900632;,
         0.390332; 0.015564;-0.920530;,
         0.245247; 0.028993;-0.968993;,
        -0.232826; 0.093539;-0.967986;,
        -0.421949; 0.031861;-0.906034;,
        -0.417615; 0.119968;-0.900632;,
        -0.732749;-0.524583;-0.433424;,
        -0.814325;-0.374706;-0.443159;,
        -0.690878;-0.490890;-0.530717;,
        -0.814325;-0.374706;-0.443159;,
        -0.759484;-0.350383;-0.548051;,
        -0.690878;-0.490890;-0.530717;,
        -0.690878;-0.490890;-0.530717;,
        -0.759484;-0.350383;-0.548051;,
        -0.573260;-0.367199;-0.732444;,
        -0.759484;-0.350383;-0.548051;,
        -0.606952;-0.276498;-0.745079;,
        -0.573260;-0.367199;-0.732444;,
        -0.573260;-0.367199;-0.732444;,
        -0.606952;-0.276498;-0.745079;,
        -0.397626;-0.261788;-0.879360;,
        -0.606952;-0.276498;-0.745079;,
        -0.416425;-0.169927;-0.893124;,
        -0.397626;-0.261788;-0.879360;,
        -0.906919; 0.180517;-0.380627;,
        -0.842555; 0.138035;-0.520585;,
        -0.901669;-0.032136;-0.431196;,
        -0.842555; 0.138035;-0.520585;,
        -0.837855;-0.045503;-0.543962;,
        -0.901669;-0.032136;-0.431196;,
        -0.901669;-0.032136;-0.431196;,
        -0.837855;-0.045503;-0.543962;,
        -0.814325;-0.374706;-0.443159;,
        -0.837855;-0.045503;-0.543962;,
        -0.759484;-0.350383;-0.548051;,
        -0.814325;-0.374706;-0.443159;;
        44;
        3;0;1;2;,
        3;3;4;5;,
        3;6;7;8;,
        3;9;10;11;,
        3;12;13;14;,
        3;15;16;17;,
        3;18;19;20;,
        3;21;22;23;,
        3;24;25;26;,
        3;27;28;29;,
        3;30;31;32;,
        3;33;34;35;,
        3;36;37;38;,
        3;39;40;41;,
        3;42;43;44;,
        3;45;46;47;,
        3;48;49;50;,
        3;51;52;53;,
        3;54;55;56;,
        3;57;58;59;,
        3;60;61;62;,
        3;63;64;65;,
        3;66;67;68;,
        3;69;70;71;,
        3;72;73;74;,
        3;75;76;77;,
        3;78;79;80;,
        3;81;82;83;,
        3;84;85;86;,
        3;87;88;89;,
        3;90;91;92;,
        3;93;94;95;,
        3;96;97;98;,
        3;99;100;101;,
        3;102;103;104;,
        3;105;106;107;,
        3;108;109;110;,
        3;111;112;113;,
        3;114;115;116;,
        3;117;118;119;,
        3;120;121;122;,
        3;123;124;125;,
        3;126;127;128;,
        3;129;130;131;;
      } //End of Mesh Normals
      MeshMaterialList { //Mesh Material List
        1;
        44;
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0;;
        Material mat_2 {
           0.640000; 0.640000; 0.640000; 1.000000;;
           92.156863;
           0.250000; 0.250000; 0.250000;;
           0.000000; 0.000000; 0.000000;;
          TextureFilename {"Textures/naty_1_0.tga";}
        }
      } //End of Mesh Material List
      MeshTextureCoords { //Mesh UV Coordinates
        132;
         0.526914; 0.025442;,
         0.852232; 0.003758;,
         0.574098; 0.090530;,
         0.852232; 0.003758;,
         0.903566; 0.088680;,
         0.574098; 0.090530;,
         0.548000; 0.179500;,
         0.574098; 0.090530;,
         0.903566; 0.088680;,
         0.903566; 0.088680;,
         0.869246; 0.188535;,
         0.548000; 0.179500;,
         0.526914; 0.025442;,
         0.574098; 0.090530;,
         0.181900; 0.061369;,
         0.181900; 0.061369;,
         0.574098; 0.090530;,
         0.197308; 0.105018;,
         0.181900; 0.061369;,
         0.197308; 0.105018;,
        -0.008944; 0.084721;,
         0.197308; 0.105018;,
         0.134477; 0.118655;,
        -0.008944; 0.084721;,
         0.197308; 0.105018;,
         0.574098; 0.090530;,
         0.548000; 0.179500;,
         0.197308; 0.105018;,
         0.548000; 0.179500;,
         0.159354; 0.149995;,
         0.159000; 0.150000;,
         0.548000; 0.179500;,
         0.197000; 0.105000;,
         0.548000; 0.179500;,
         0.574000; 0.090500;,
         0.197000; 0.105000;,
         0.197000; 0.105000;,
         0.574000; 0.090500;,
         0.182000; 0.061375;,
         0.574000; 0.090500;,
         0.527000; 0.025500;,
         0.182000; 0.061375;,
         0.182000; 0.061375;,
         0.527000; 0.025500;,
         0.133000; 0.027000;,
         0.527000; 0.025500;,
         0.447000;-0.030250;,
         0.133000; 0.027000;,
         0.447000;-0.030250;,
         0.527000; 0.025500;,
         0.739000;-0.064250;,
         0.527000; 0.025500;,
         0.852000; 0.003750;,
         0.739000;-0.064250;,
         0.739000;-0.064250;,
         0.852000; 0.003750;,
         1.033000;-0.089750;,
         0.852000; 0.003750;,
         1.167000;-0.010380;,
         1.033000;-0.089750;,
         0.527000; 0.025500;,
         0.574000; 0.090500;,
         0.852000; 0.003750;,
         0.574000; 0.090500;,
         0.904000; 0.088625;,
         0.852000; 0.003750;,
         0.852000; 0.003750;,
         0.904000; 0.088625;,
         1.167000;-0.010380;,
         0.904000; 0.088625;,
         1.224000; 0.088250;,
         1.167000;-0.010380;,
         0.574000; 0.090500;,
         0.548000; 0.179500;,
         0.904000; 0.088625;,
         0.548000; 0.179500;,
         0.869000; 0.188500;,
         0.904000; 0.088625;,
         0.904000; 0.088625;,
         0.869000; 0.188500;,
         1.224000; 0.088250;,
         0.869000; 0.188500;,
         1.173000; 0.197250;,
         1.224000; 0.088250;,
         0.134000; 0.118625;,
         0.197000; 0.105000;,
         0.182000; 0.061375;,
         0.134000; 0.118625;,
         0.182000; 0.061375;,
        -0.009000; 0.084750;,
         0.133001; 0.027031;,
         0.181900; 0.061369;,
        -0.008944; 0.084721;,
        -0.009000; 0.084750;,
         0.182000; 0.061375;,
         0.133000; 0.027000;,
         0.159000; 0.150000;,
         0.197000; 0.105000;,
         0.134000; 0.118625;,
         0.134477; 0.118655;,
         0.197308; 0.105018;,
         0.159354; 0.149995;,
         1.033260;-0.089700;,
         1.166890;-0.010370;,
         0.739033;-0.064190;,
         1.166890;-0.010370;,
         0.852232; 0.003758;,
         0.739033;-0.064190;,
         0.739033;-0.064190;,
         0.852232; 0.003758;,
         0.446892;-0.030250;,
         0.852232; 0.003758;,
         0.526914; 0.025442;,
         0.446892;-0.030250;,
         0.446892;-0.030250;,
         0.526914; 0.025442;,
         0.133001; 0.027031;,
         0.526914; 0.025442;,
         0.181900; 0.061369;,
         0.133001; 0.027031;,
         1.173110; 0.197231;,
         0.869246; 0.188535;,
         1.224140; 0.088271;,
         0.869246; 0.188535;,
         0.903566; 0.088680;,
         1.224140; 0.088271;,
         1.224140; 0.088271;,
         0.903566; 0.088680;,
         1.166890;-0.010370;,
         0.903566; 0.088680;,
         0.852232; 0.003758;,
         1.166890;-0.010370;;
      } //End of Mesh UV Coordinates
    } //End of Mesh Mesh
  } //End of naty_mat_2
} //End of Root Frame
