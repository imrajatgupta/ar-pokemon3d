# AR-Pokemon 3D

An Augmented Reality based 3D Pokemon creating iOS App.


## Requirements

1) [Mac OS 10.0+](https://developer.apple.com/macos/)

2) AR/VR capable device (iPad Pro 2018 or newer, iPhone 10 or newer)

3) Xcode 10.0+

4) SceneKit

5) ARKit

6) CoreML


### Testing

**System Configuration:**

Model: MacBook Pro (13-inch, 2018, Four Thunderbolt 3 Ports)

Processor: 2.3 GHz Intel Core i5

Memory: 8 GB 2133 MHz LPDDR3

Graphics: Intel Iris Plus Graphics 655 1536 MB


**Testing device:**

Model: iPad Pro (11-inch, 2018, USB-C)

OS: [iOS 12](https://developer.apple.com/ios/)

Memory: 64 GB


**Software:**

1) [Xcode 10.2.1](https://developer.apple.com/xcode/)

2) [Usdz File Converter](https://github.com/franklinzhu/usdz-file-converter)

3) [ARKit](https://developer.apple.com/documentation/arkit)

4) [CoreML](https://developer.apple.com/documentation/coreml)

5) [SceneKit](https://developer.apple.com/scenekit/)


**Language:**

[Swift 4](https://developer.apple.com/documentation/swift)





